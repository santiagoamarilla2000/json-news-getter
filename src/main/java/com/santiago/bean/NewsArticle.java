package com.santiago.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class NewsArticle {
    @Schema(description = "News creation date", example="2022-07-14T23:18:16.4725114")
    @JsonProperty("date")
    private String dateString;

    @Schema(description = "News link", example="https://www.popular.com.py/")
    private String link;

    @JsonProperty("photo_link")
    @Schema(description = "News photo link", example="https://www.popular.com.py/foto.png" )
    private String photoLink;

    @Schema(description = "News title", example="Generic New 01")
    private String title;

    @Schema(description = "News summary", example="This is today's most important news")
    private String summary;

    @Schema(description = "Base64 byte array of news photo", example="NlaHFIcDE0a1B1TldHaXFFQkcrRnRVWldXYngyaGFCc1Vjc...")
    @JsonProperty("photo_content")
    private byte[] photoContent;

    @Schema(description = "News photo content type", example="image/png")
    @JsonProperty("photo_content_type")
    private String photoContentType;
}
