package com.santiago.bean;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class Error {
    @Schema(description = "Error code", example="gXYZ")
    String code;
    @Schema(description = "Error message", example="Failed to obtain news")
    String message;
}
