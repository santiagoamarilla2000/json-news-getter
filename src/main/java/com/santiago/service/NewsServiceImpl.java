package com.santiago.service;

import com.santiago.bean.Error;
import com.santiago.bean.NewsArticle;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Formatter;

@Service
public class NewsServiceImpl implements NewsService{

    private static final Logger LOGGER = LoggerFactory.getLogger(NewsServiceImpl.class);

    @Value("${api.key}")
    private String localApiKey;
    @Value("${news.url}")
    private String newsUrl;
    @Value("${news.query.param}")
    private String newsQueryParam;
    @Value("${news.item.class.list}")
    private String newsItemClassList;
    @Value("${news.item.antiquity.class.list}")
    private String newsItemAntiquityClassList;
    @Value("${news.item.image.class.list}")
    private String newsItemImageClassList;

    @Override
    public ResponseEntity<?> processGetNewsArray(String apiKey, String queryParam, Boolean mustGetPhotos) {
        LOGGER.debug("start processGetNewsArray");
        ResponseEntity <?> responseEntity;
        if(mustGetPhotos == null) mustGetPhotos = false;
        try {
            if (isValidApiKey(apiKey)) {
                ArrayList<NewsArticle> newsArticleList = getNewsArticleListFromWebsite(queryParam, mustGetPhotos);
                if (newsArticleList.isEmpty()) {
                    responseEntity = new ResponseEntity <  > (getErrorToReturn(HttpStatus.NOT_FOUND, queryParam), HttpStatus.NOT_FOUND);
                } else {
                    HttpHeaders responseHeaders = new HttpHeaders();
                    responseHeaders.set("signed-api-key", calculateHMAC(newsArticleList.toString(), localApiKey));
                    responseEntity = new ResponseEntity <> (newsArticleList, responseHeaders, HttpStatus.OK);
                }
            } else {
                responseEntity = new ResponseEntity <> (getErrorToReturn(HttpStatus.FORBIDDEN, ""), HttpStatus.FORBIDDEN);
            }

        } catch (Throwable th) {
            LOGGER.error("Error in getNewsArray: ", th);
            responseEntity = new ResponseEntity <> (getErrorToReturn(HttpStatus.INTERNAL_SERVER_ERROR, ""), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    private static String calculateHMAC(String data, String key) throws Throwable {
        String hexString = "";
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKeySpec);
            hexString = toHexString(mac.doFinal(data.getBytes()));
        } catch (Throwable th) {
            LOGGER.error("Error in calculateHMAC: ", th);
        }
        return hexString;
    }

    private static String toHexString(byte[] bytes) {
        try (Formatter formatter = new Formatter()) {
            for (byte b: bytes) {
                formatter.format("%02x", b);
            }
            return formatter.toString();
        }
    }

    private boolean isValidApiKey(String apiKey) {
        boolean isValid = false;
        try {
            isValid = (apiKey.equals(localApiKey));
        } catch (Throwable th) {
            LOGGER.error("Error in isValidApiKey", th);
            throw th;
        }
        return isValid;
    }

    private static Error getErrorToReturn(HttpStatus httpStatus, String queryParam) {
        Error errorToReturn = new Error();

        switch (httpStatus) {
            case NOT_FOUND:
                errorToReturn.setCode("g267");
                errorToReturn.setMessage("No se encuentran noticias para el texto: " + queryParam);
                break;
            case BAD_REQUEST:
                errorToReturn.setCode("g268");
                errorToReturn.setMessage("Parámetros inválidos.");
                break;
            case INTERNAL_SERVER_ERROR:
                errorToReturn.setCode("g100");
                errorToReturn.setMessage("Error interno del servidor.");
                break;
            case FORBIDDEN:
                errorToReturn.setCode("g103");
                errorToReturn.setMessage("No autorizado.");
                break;
            default:
                errorToReturn.setCode("0");
                errorToReturn.setMessage("Error no manejado: " + httpStatus);
                break;
        }
        return errorToReturn;
    }

    private ArrayList < NewsArticle > getNewsArticleListFromWebsite(String queryParamValue, boolean mustGetPhotos) throws Throwable {
        ArrayList < NewsArticle > newsArticleArrayList = new ArrayList < > ();
        NewsArticle newsArticle;
        LocalDateTime localDateTime = LocalDateTime.now();
        try {
            //codificamos parametro y armamos url
            queryParamValue = URLEncoder.encode(queryParamValue, StandardCharsets.UTF_8);
            String url = newsUrl + newsQueryParam + queryParamValue;

            Document doc = Jsoup.connect(url).get();

            Elements newsItems = doc.getElementsByClass(newsItemClassList);

            for (Element newsItem: newsItems) {

                newsArticle = new NewsArticle();
                newsArticle.setDateString(getNewsItemDate(localDateTime, newsItem.getElementsByClass(newsItemAntiquityClassList).get(0)));
                newsArticle.setLink(newsItem.getElementsByTag("a").get(0).attr("href"));
                newsArticle.setPhotoLink(newsItem.getElementsByClass(newsItemImageClassList).get(0).attr("src"));
                newsArticle.setTitle(newsItem.getElementsByTag("h2").get(0).text());
                newsArticle.setSummary(newsItem.getElementsByTag("p").get(0).text());

                if (mustGetPhotos) {
                    setPhotoData(newsArticle, newsItem.getElementsByClass(newsItemImageClassList).get(0).attr("src"));
                }
                newsArticleArrayList.add(newsArticle);
            }
        } catch (Throwable th) {
            LOGGER.error("Error in getNewsArrayFromWebsite", th);
            throw th;
        }
        LOGGER.debug("newsArticleArrayList: {}", newsArticleArrayList);
        return newsArticleArrayList;
    }

    private static void setPhotoData(NewsArticle newsArticle, String newsArticleImageUrl) throws Throwable {
        InputStream inputStream = null;
        byte[] imageBytes;
        byte[] encodedImageBytes;
        URL url = null;
        try {
            url = new URL(newsArticleImageUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int status = con.getResponseCode();

            if (status == HttpStatus.OK.value()) {
                inputStream = url.openStream();
                imageBytes = IOUtils.toByteArray(inputStream);
                encodedImageBytes = Base64.getEncoder().encode(imageBytes);
                newsArticle.setPhotoContent(encodedImageBytes);
                newsArticle.setPhotoContentType(con.getHeaderField("Content-Type"));

            } else {
                throw new Exception("Error al obtener foto");
            }


        } catch (IOException e) {
            if(url != null){
                LOGGER.error("Failed while reading bytes from {}", url.toExternalForm(), e);
            }else{
                LOGGER.error("Failed while parsing url", e);
            }
            throw e;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }


    private static String getNewsItemDate(LocalDateTime localDateTime, Element antiquitySpan) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;
        LocalDateTime dateTimeToReturn = localDateTime;

        try {
            //Este valor es un texto seguido de un nro seguido de día/semana/mes/año, ejemplo: 'hace 2 días'
            String antiquity = antiquitySpan.text();
            int antiquityValue = Integer.parseInt(antiquity.substring(antiquity.indexOf(" ") + 1, antiquity.lastIndexOf(" ")));

            if (antiquity.contains("día")) {
                dateTimeToReturn = dateTimeToReturn.minusDays(antiquityValue);
            } else if (antiquity.contains("semana")) {
                dateTimeToReturn = dateTimeToReturn.minusWeeks(antiquityValue);
            } else if (antiquity.contains("mes")) {
                dateTimeToReturn = dateTimeToReturn.minusMonths(antiquityValue);
            } else if (antiquity.contains("año")) {
                dateTimeToReturn = dateTimeToReturn.minusYears(antiquityValue);
            }
        } catch (Throwable th) {
            LOGGER.error("Error in getNewsItemDate", th);
        }

        return dateTimeFormatter.format(dateTimeToReturn);
    }
}
