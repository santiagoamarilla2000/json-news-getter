package com.santiago.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


public interface NewsService {
    ResponseEntity<?>  processGetNewsArray(String apiKey, String queryParam, Boolean mustGetPhotos);
}
