package com.santiago.controller;

import com.santiago.bean.Error;
import com.santiago.bean.NewsArticle;
import com.santiago.service.NewsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/news")
public class NewsControllerImpl implements NewsController {
    @Autowired
    NewsService service;
    @Override
    @GetMapping(value = "/query", produces = "application/json")
    public ResponseEntity<?> processGetNews(@RequestHeader(name = "api_key") String apiKey,
                                            @RequestParam(name = "query_param") String queryParam,
                                            @RequestParam(name = "mustGetPhotos", required=false) Boolean mustGetPhotos) {

        return service.processGetNewsArray(apiKey, queryParam, mustGetPhotos);
    }
}
