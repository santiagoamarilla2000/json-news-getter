package com.santiago;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonNewsGetterApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonNewsGetterApplication.class, args);
	}

}
