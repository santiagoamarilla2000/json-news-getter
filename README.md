# News Getter In JSON

API that returns news on JSON format based on received parameters

## Tech Stack
- Java 17
- Spring Boot 3.3.0
- Open API 2.0.2

## Installation

1- Download the executable file JsonNewsGetter.jar

2- Execute it with the following command:

```bash
java -jar JsonNewsGetter.jar
```

## Usage

Go to [this link](http://localhost:8080/swagger-ui/index.html) to access the swagger-ui, see the documentation and use the app.
Expected api key value: xKOsdcnj18=!
